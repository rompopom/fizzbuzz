<?php

declare(strict_types=1);

namespace App\Infrastructure\UI\Web;

use App\Application\FizzBuzz;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class FizzBuzzController
 * @package App\Infrastructure\UI\Web
 */
class FizzBuzzController
{
    /**
     * @Route("/fizz-buzz", name="app_fizz_buzz")
     * @param FizzBuzz $fizzBuzz
     * @return Response
     */
    public function fizzBuzz(FizzBuzz $fizzBuzz): Response
    {
        return new Response(
            '<html lang="en"><body>' . $fizzBuzz->generate(100) . '</body></html>'
        );
    }
}
