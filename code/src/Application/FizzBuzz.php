<?php

declare(strict_types=1);

namespace App\Application;

/**
 * Class FizzBuzz
 * @package App\Application
 */
class FizzBuzz
{
    /**
     * @param int $id
     * @return string
     */
    public function generate(int $id): string
    {
        $tab = '';
        for($i = 1; $i < $id+1; $i++) {
            $output = (($i % 3 == 0) ? 'Fizz' : '') . (($i % 5 == 0) ? 'Buzz' : '');
            $tab = $tab . (!empty($output) ? $output : (string)$i) . '</br>';
        }
        return $tab;
    }
}