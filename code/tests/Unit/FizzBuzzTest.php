<?php

declare(strict_types=1);

namespace App\Tests\Unit;

use App\Application\FizzBuzz;
use PHPUnit\Framework\TestCase;

class FizzBuzzTest extends TestCase
{
    /**
     * @dataProvider provideInvalidCredentials
     * @test
     * @param $id
     * @param $output
     */
    public function testFizzBuzz($id, $output): void
    {
        $fizzBuzz = new FizzBuzz();
        $actual = $fizzBuzz->generate($id);
        $this->assertSame($output, $actual);
    }

    /**
     * @return iterable
     */
    public function provideInvalidCredentials(): iterable
    {
        yield [1, "1</br>"];
        yield [3, "1</br>2</br>Fizz</br>"];
        yield [5, "1</br>2</br>Fizz</br>4</br>Buzz</br>"];
        yield [15, "1</br>2</br>Fizz</br>4</br>Buzz</br>Fizz</br>7</br>8</br>Fizz</br>Buzz</br>11</br>Fizz</br>13</br>14</br>FizzBuzz</br>"];
    }
}