<?php

declare(strict_types=1);

namespace App\Tests\Acceptance;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class GetFizzBuzzTest extends WebTestCase
{
    public function testGetHelloWorld_returns_HelloWorld(): void
    {
        $client = static::createClient();
        $client->request('GET', '/fizz-buzz');

        self::assertEquals(200, $client->getResponse()->getStatusCode());
        self::assertStringContainsString("1</br>2</br>Fizz</br>4</br>Buzz</br>Fizz</br>7</br>8</br>Fizz</br>", $client->getResponse()->getContent());
    }
}
